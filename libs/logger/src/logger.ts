export class Logger {
  private isLogging = true;

  constructor(private readonly name: string, private readonly template = "[$time$][$name$] $message$") {}

  setIsLogging(isLogging: boolean) {
    this.isLogging = isLogging;
  }

  debug(message: string) {
    this.isLogging && console.debug(this.handleTemplate(message));
  }

  info(message: string) {
    this.isLogging && console.info(this.handleTemplate(message));
  }

  log(message: string) {
    this.isLogging && console.log(this.handleTemplate(message));
  }

  warn(message: string) {
    this.isLogging && console.warn(this.handleTemplate(message));
  }

  error(message: string) {
    this.isLogging && console.error(this.handleTemplate(message));
  }

  private handleTemplate(message: string) {
    return this.template.replace("$time$", new Date().toISOString()).replace("$name$", this.name).replace("$message$", message);
  }
}
