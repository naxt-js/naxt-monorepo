export interface CliBaseOption {
  name: string;
  description?: string;
  alias?: string;
  choices?: string[];
  isArray?: boolean;
}

export interface CliStringOption extends CliBaseOption {
  type: "string";
  default?: string;
}

export interface CliBooleanOption extends CliBaseOption {
  type: "boolean";
  default?: boolean;
}

export interface CliNumberOption extends CliBaseOption {
  type: "number";
  default?: number;
}

export type CliOption = CliStringOption | CliBooleanOption | CliNumberOption;
export type CliOptions = CliOption[];
