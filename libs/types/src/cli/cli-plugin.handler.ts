import { Promisify } from "../utils";
import { CliPlugin } from "./plugin";

export type CliPluginHandler = () => Promisify<CliPlugin>;
export type CliPluginHandlers = CliPluginHandler[];
