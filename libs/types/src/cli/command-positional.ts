export interface CommandPositionalBaseOptions {
  description?: string;
}

export interface CommandPositionalStringOptions extends CommandPositionalBaseOptions {
  type: "string";
  default?: string;
  choices?: string[];
}

export interface CommandPositionalBooleanOptions extends CommandPositionalBaseOptions {
  type: "boolean";
  default?: boolean;
  choices?: true[];
}

export interface CommandPositionalNumberOptions extends CommandPositionalBaseOptions {
  type: "number";
  default?: number;
  choices?: number[];
}

export type CommandPositionalOptions = CommandPositionalStringOptions | CommandPositionalBooleanOptions | CommandPositionalNumberOptions;
