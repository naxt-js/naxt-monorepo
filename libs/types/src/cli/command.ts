import { RunnerFunction } from "./runner-function";
import { CommandPositionalOptions } from "./command-positional";
import { CliOption } from "./cli-options";
import { AppConfig } from "../config";

export interface Command {
  name: string;
  aliases?: string[];
  description?: string;
  positional?: Record<string, Partial<CommandPositionalOptions>>;
  options?: Record<string, Omit<CliOption, "name">>;

  runner(config: AppConfig): RunnerFunction;
}

export type Commands = Command[];
