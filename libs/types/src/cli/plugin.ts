import { Commands } from "@naxt/types";

export interface CliPlugin {
  name: string;
  commands: Commands;
}
