import { ArgumentsCamelCase } from "yargs";
import { Promisify } from "../utils";

export type RunnerFunction = <T>(args: ArgumentsCamelCase<T>) => Promisify<void>;
export type RunnerFunctions = RunnerFunction[];
