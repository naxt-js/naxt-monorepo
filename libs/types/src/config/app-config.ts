import { RollupPlugins } from "../rollup";
import { BuildOptions } from "./build-options";

export interface AppConfig {
  /**
   * The directory to output the build to.
   */
  rootDir: string;

  /**
   * Entry file
   */
  entry: string | string[];

  /**
   * Naxt build options
   */
  build: BuildOptions;

  /**
   * Naxt global plugins
   */
  plugins: RollupPlugins;
}
