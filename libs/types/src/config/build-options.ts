import { MinifyOptions } from "terser";
import { Options as PrettierOptions } from "prettier";

import { RollupPlugins } from "../rollup";
import { JsonOptions } from "./json-options";

export interface BuildOptions {
  /**
   * The directory to output the build to.
   */
  dir: string;

  /**
   * App title.
   * @default package.json name
   */
  title: string;

  /**
   * These plugins for build step
   */
  plugins: RollupPlugins;

  /**
   * JSON options
   */
  json: JsonOptions;

  /**
   * For production environment only.
   * If true, code will be minified.
   */
  minify?: MinifyOptions | true;

  /**
   * For development environment only.
   * If true, code will be formatted.
   */
  prettier?: PrettierOptions;
}
