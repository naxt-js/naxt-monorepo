import { AppConfig } from "@naxt/types";

export interface InternalAppConfig extends AppConfig {
  /**
   * The root directory of the application
   */
  isBuild: boolean;
}
