export interface JsonOptions {
  /**
   * For development environment only.
   * Indentation as a.json string of spaces or tabs, or as a.json number of spaces.
   */
  indent: number | string;

  /**
   * If true, the JSON will be stringifies and export as default JSON.parse(stringifiedJSON).
   * If false, the JSON will be separated and exported as key/value.
   */
  stringify: boolean;

  /**
   * For development environment only.
   * If true, the JSON will be exported as named exports.
   * If false, the JSON will be exported as default.
   */
  namedExports: boolean;
}
