import { RollupOptions as _RollupOptions } from "rollup";

export interface RollupOptions extends _RollupOptions {}
