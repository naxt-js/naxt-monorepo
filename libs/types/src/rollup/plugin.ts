import { Plugin } from "rollup";

export interface RollupPlugin extends Plugin {
  enforce?: "pre" | "post";
}

export type RollupPlugins = RollupPlugin[];
