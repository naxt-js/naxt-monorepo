import chalk from "chalk";
import { Logger } from "@naxt/logger";

export class StringBuilder {
  private string = ``;
  private loggerString = ``;
  private stringSeparator = ``;

  private logger: Console | Logger = console;

  red(str: string | string[]) {
    return this.append(chalk.red(str));
  }

  redHeader(str: string | string[]) {
    return this.append(chalk.bold.red(str));
  }

  blue(str: string | string[]) {
    return this.append(chalk.blue(str));
  }

  blueHeader(str: string | string[]) {
    return this.append(chalk.bold.blue(str));
  }

  green(str: string | string[]) {
    return this.append(chalk.green(str));
  }

  greenHeader(str: string | string[]) {
    return this.append(chalk.bold.green(str));
  }

  yellow(str: string | string[]) {
    return this.append(chalk.yellow(str));
  }

  yellowHeader(str: string | string[]) {
    return this.append(chalk.bold.yellow(str));
  }

  cyan(str: string | string[]) {
    return this.append(chalk.cyan(str));
  }

  cyanHeader(str: string | string[]) {
    return this.append(chalk.bold.cyan(str));
  }

  magenta(str: string | string[]) {
    return this.append(chalk.magenta(str));
  }

  magentaHeader(str: string | string[]) {
    return this.append(chalk.bold.magenta(str));
  }

  white(str: string | string[]) {
    return this.append(chalk.white(str));
  }

  whiteHeader(str: string | string[]) {
    return this.append(chalk.bold.white(str));
  }

  gray(str: string | string[]) {
    return this.append(chalk.gray(str));
  }

  grayHeader(str: string | string[]) {
    return this.append(chalk.bold.gray(str));
  }

  black(str: string | string[]) {
    return this.append(chalk.black(str));
  }

  blackHeader(str: string | string[]) {
    return this.append(chalk.bold.black(str));
  }

  header(str: string) {
    return this.append(chalk.bold(str));
  }

  headerIf(condition: boolean, str: string) {
    if (condition) this.header(str);
    return this;
  }

  spaces(count = 1) {
    return this.append(" ".repeat(count));
  }

  spacesIf(condition: boolean, count = 1) {
    if (condition) this.spaces(count);
    return this;
  }

  separator(separator: string) {
    this.stringSeparator = separator;
    return this;
  }

  newLine() {
    return this.append(`\n`);
  }

  newLineIf(condition: boolean) {
    if (condition) this.newLine();
    return this;
  }

  log() {
    this.logger.log(this.loggerString);
    this.loggerString = ``;
    this.string += "\n";
    return this;
  }

  setLogger(logger: Logger) {
    this.logger = logger;
    return this;
  }

  append(str: string | number | boolean) {
    this.string += this.stringSeparator + str;
    this.loggerString += str;
    return this;
  }

  appendIf(condition: boolean, str: string) {
    if (condition) this.append(str);
    return this;
  }

  appendGroup<T>(handler: (data: T, index: number) => string, group: Set<T> | T[]) {
    group.forEach((data, index) => {
      this.append(handler(data, index));
    });
    return this;
  }

  appendGroupIf<T>(handler: (data: T) => string, group: Set<T> | T[], condition: boolean) {
    if (condition) this.appendGroup(handler, group);
    return this;
  }

  prepend(str: string) {
    this.string = this.stringSeparator + str + this.string;
    this.loggerString = str + this.loggerString;
    return this;
  }

  prependIf(condition: boolean, str: string) {
    if (condition) this.prepend(str);
    return this;
  }

  tick() {
    return this.append("✔");
  }

  tickIf(condition: boolean) {
    if (condition) this.tick();
    return this;
  }

  cross() {
    return this.append("✖");
  }

  crossIf(condition: boolean) {
    if (condition) this.cross();
    return this;
  }

  clear() {
    console.clear();
    return this;
  }

  build() {
    return this.string.trim();
  }
}

export type Colors = "red" | "blue" | "green" | "yellow" | "cyan" | "magenta" | "white" | "gray" | "black";
const stringBuilder = (color: Colors) => (str: string) => new StringBuilder()[color]([str]).build();

export const red = stringBuilder("red");
export const blue = stringBuilder("blue");
export const green = stringBuilder("green");
export const yellow = stringBuilder("yellow");
export const cyan = stringBuilder("cyan");
export const magenta = stringBuilder("magenta");
export const white = stringBuilder("white");
export const gray = stringBuilder("gray");
export const black = stringBuilder("black");
