export const POSITIONAL_OPTION_REGEX = /^([\[<])([^\s:=]+)(:[^\s=]+)?(=\S+)?([>\]])$/;
export const VALIDATE_NUMBER_REGEX = /^-?\d+$/;
export const VALIDATE_BOOLEAN_REGEX = /^(true|false)$/;

export const CSS_IMPORT_REGEX = /@import\s+(?:url\()?['"]([^'"]+)['"]\)?;?/;
export const URL_REGEX = /(\b(?:https?|ftp|file):\/\/)[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+[-A-Za-z0-9+&@#\/%=~_|]/;
