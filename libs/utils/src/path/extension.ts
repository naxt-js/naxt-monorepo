import { Path } from ".";

export class Extension {
  constructor(private path: Path) {}

  static from(path: Path) {
    return new Extension(path);
  }

  isSameTo(extension: string) {
    return this.path.path.endsWith(extension.startsWith(".") ? extension : `.${extension}`);
  }

  isSameOneOf(extensions: string[]) {
    return extensions.some(extension => this.isSameTo(extension));
  }

  setExtension(extension: string) {
    this.path.path = this.path.path + (extension.startsWith(".") ? extension : `.${extension}`);
    return this.path;
  }

  getExtension<T = string>() {
    return this.path.path.split(".").at(-1) as T;
  }

  removeExtension(countOfExtensions = 1) {
    this.path.path = this.path.path.split(".").slice(0, -countOfExtensions).join(".");
    return this.path;
  }
}
