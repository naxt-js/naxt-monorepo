import { basename, dirname, relative, resolve } from "path";
import { copyFileSync, existsSync, statSync } from "fs";
import { PathResolver } from "./path-resolver";
import { Extension } from "./extension";
import { Source } from "./source";

export * from "./extension";
export * from "./path-resolver";
export * from "./source";

export class Path {
  constructor(path: string, root: string | Path = process.cwd()) {
    this.path = path;
    this.root = root instanceof Path ? root.fullPath : root;
  }

  // region field-getter-setters

  private _root: string;

  get root() {
    return this._root;
  }

  set root(value: string) {
    this._root = value;
  }

  private _path: string;

  get path(): string {
    return this._path;
  }

  set path(value: string) {
    this._path = value;
  }

  // endregion

  // region getters

  get fullPath() {
    return resolve(this.root, this._path);
  }

  get importPath() {
    return this.fullPath.split(/[\\/]/).join("/");
  }

  get dirname() {
    return new Path(dirname(this.fullPath));
  }

  get basename() {
    return basename(this.path);
  }

  get lastDir() {
    return !this.exists ? this : this.isFile ? this.dirname : this;
  }

  get stats() {
    return statSync(this.fullPath);
  }

  get isFile() {
    return this.stats.isFile();
  }

  get isDirectory() {
    return this.stats.isDirectory();
  }

  get exists() {
    return existsSync(this.fullPath);
  }

  // endregion

  // region extra functions

  get extension() {
    return Extension.from(this);
  }

  get resolve() {
    return PathResolver.from(this);
  }

  get source() {
    return new Source(this);
  }

  // endregion

  // region static methods

  static from(path: string, root: string | Path = process.cwd()) {
    return new Path(path, root);
  }

  static resolveFiles(rootDir: string, files: string[]) {
    return files.map(file => this.from(file, rootDir));
  }

  // endregion

  // region methods

  duplicateTo(path: string) {
    return new Path(path, this.lastDir);
  }

  /**
   * Get relative path from argument path to this path
   * @param {string | Path} path
   * @param {boolean} asUnixSystem
   */
  relativeTo(path: string | Path, asUnixSystem = false) {
    let newPath = relative(path instanceof Path ? path.fullPath : path, this.fullPath);

    if (asUnixSystem) {
      newPath = newPath.split(/[\\/]/).join("/");
    }

    return newPath;
  }

  copyTo(newCopiedPath: Path | string) {
    if (newCopiedPath instanceof Path) {
      newCopiedPath = newCopiedPath.fullPath;
    }
    Path.from(newCopiedPath).dirname.source.mkdir();

    copyFileSync(this.fullPath, newCopiedPath);
  }
}
