import { Path } from ".";
import glob, { IOptions } from "glob";

interface GlobOptions<T> extends Omit<IOptions, "cwd" | "absolute"> {
  asPath?: T;
}

export class PathResolver {
  private resolvedPath: string;

  constructor(private path: Path) {}

  static from(path: Path) {
    return new PathResolver(path);
  }

  setPath(resolvedPath: string) {
    this.resolvedPath = resolvedPath;
    return this;
  }

  findByExtension(extension: string): Path {
    const resolvedPath = this.path.duplicateTo(this.resolvedPath);
    resolvedPath.extension.setExtension(extension);

    if (!resolvedPath.exists) {
      throw new Error(`Could not find ${resolvedPath.path}`);
    }

    return resolvedPath;
  }

  findByExtensions(extensions: string[]) {
    let foundExtensionPath: Path = null;
    for (const extension of extensions) {
      try {
        foundExtensionPath = this.findByExtension(extension);
        if (foundExtensionPath) break;
      } catch (e) {}
    }

    if (!foundExtensionPath) {
      const extensionsString = extensions.join(", ");
      const errMess = `Could not find any of the following extensions: ${extensionsString} in ${this.path.path}`;
      throw new Error(errMess);
    }

    return foundExtensionPath;
  }

  findFile() {
    if (this.path.exists) return this.path;

    const dirname = this.path.dirname;
    const foundFile = dirname.source.getFiles().find(file => {
      const path = Path.from(file);
      path.extension.removeExtension();
      return path.basename === this.path.basename;
    });

    return dirname.duplicateTo(foundFile);
  }

  glob(options?: GlobOptions<true>): Path[];
  glob(options?: GlobOptions<false>): string[];
  glob(options: GlobOptions<boolean> = {}) {
    const { asPath = false, ...globOptions } = options;
    const globPaths = glob.sync(this.path.path, { cwd: this.path.root, absolute: asPath, ...globOptions });
    return globPaths.map(path => (asPath ? Path.from(path) : path));
  }
}
