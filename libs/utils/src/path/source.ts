import { mkdirSync, readdirSync, readFileSync, writeFileSync } from "fs";
import rimraf from "rimraf";

import { Path } from ".";
import { Promisify } from "@naxt/types";

export class Source {
  constructor(private path: Path) {}

  static from(path: Path) {
    return new Source(path);
  }

  read() {
    if (!this.path.isFile) {
      throw new Error(`${this.path.fullPath} is not a file`);
    }

    return readFileSync(this.path.fullPath, "utf8");
  }

  readBytes() {
    if (!this.path.isFile) {
      throw new Error(`${this.path.fullPath} is not a file`);
    }

    return readFileSync(this.path.fullPath);
  }

  readJson<T>() {
    return JSON.parse(this.read()) as T;
  }

  getFiles() {
    if (!this.path.isDirectory) {
      throw new Error(`${this.path.fullPath} is not a directory`);
    }

    return readdirSync(this.path.fullPath);
  }

  mkdir(path = this.path.lastDir) {
    mkdirSync(path.fullPath, { recursive: true });
    return path;
  }

  write(content: string | Buffer) {
    try {
      this.mkdir(this.path.isFile ? this.path.dirname : this.path);
    } catch {
      this.mkdir(this.path.dirname);
    }

    writeFileSync(this.path.fullPath, content);
  }

  rmdir() {
    rimraf.sync(this.path.fullPath);
  }

  readAndParse<T extends object>(): Promisify<T> {
    if (this.path.extension.isSameOneOf(["js", "ts", "json"])) {
      return import(this.path.fullPath);
    }

    if (this.path.extension.isSameOneOf(["yaml", "yml"])) {
      return import("js-yaml").then(yaml => yaml.load(this.read()) as T);
    }

    if (this.path.extension.isSameOneOf(["toml"])) {
      return import("toml").then(toml => toml.parse(this.read()) as T);
    }

    return this.readJson<T>();
  }
}
