export * from "./builder";
export * from "./helpers";
export * from "./path";

export const isDev = process.env.NODE_ENV === "development";
export const isCliDev = process.env.CLI_DEV === "true";
