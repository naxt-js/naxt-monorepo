import { isCliDev, Path } from "@naxt/utils";
import { hideBin } from "yargs/helpers";

import { YargsHelper } from "./utils/yargs-helper";
import { pluginHandler } from "./utils/plugin-handler";

export const cli = async (args = hideBin(process.argv), root = process.cwd()) => {
  const yargsHelper = new YargsHelper();
  const indexOfDemoProject = args.indexOf("--demo-project");
  const indexOfDemoProjectAlias = args.indexOf("-d");
  const demoProjectIndex = indexOfDemoProject > -1 ? indexOfDemoProject : indexOfDemoProjectAlias;

  if (demoProjectIndex > -1) {
    root = isCliDev && args[demoProjectIndex + 1] ? Path.from(args[demoProjectIndex + 1], process.cwd()).fullPath : root;
    args.splice(demoProjectIndex, 2);
  }

  await pluginHandler(yargsHelper, root);

  yargsHelper.build().parse(args);
};

isCliDev && cli().then(null);
