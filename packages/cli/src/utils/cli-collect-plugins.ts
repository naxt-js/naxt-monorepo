import { CliPluginHandlers } from "@naxt/types";
import { Path } from "@naxt/utils";

export const collectCliPlugins = async () => {
  const rootPkgJson = await import(Path.from("package.json").fullPath);
  const internalPackages = Path.from("node_modules/@naxt")
    .source.getFiles()
    .reduce((acc, a) => {
      if (!a.startsWith("demo")) {
        acc[`@naxt/${a}`] = require(`@naxt/${a}/package.json`).version;
      }
      return acc;
    }, {});

  const rootPkgJsonAllDeps = {
    ...(rootPkgJson.dependencies || {}),
    ...(rootPkgJson.devDependencies || {}),
    ...internalPackages
  };

  return Object.keys(rootPkgJsonAllDeps).reduce((acc, pkgName) => {
    try {
      const pkgJson = require(`${pkgName}/package.json`);

      if (pkgJson.main) {
        const pkg = require(pkgName);
        if ("naxtCliPlugin" in pkg) {
          acc.push(pkg.naxtCliPlugin);
        }
      }
    } catch {}

    return acc;
  }, [] as CliPluginHandlers);
};
