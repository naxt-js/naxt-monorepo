import { resolveConfig } from "@naxt/config";
import { collectCliPlugins } from "./cli-collect-plugins";
import { Assigns, CommandPositionalOptions } from "@naxt/types";
import { YargsHelper } from "./yargs-helper";
import { POSITIONAL_OPTION_REGEX, VALIDATE_BOOLEAN_REGEX, VALIDATE_NUMBER_REGEX } from "@naxt/utils";

export const pluginHandler = async (yargsHelper: YargsHelper, root: string) => {
  const config = await resolveConfig(root);
  const pluginNames = new Set<string>();
  const cliPlugins = await collectCliPlugins();

  for (const cliPlugin of cliPlugins) {
    const plugin = await cliPlugin();

    if (pluginNames.has(plugin.name)) {
      throw new Error(`Plugin name "${plugin.name}" already exists.`);
    }
    pluginNames.add(plugin.name);

    for (const command of plugin.commands) {
      const [commandName, ...positionalCommands] = command.name.split(" ").filter(Boolean);
      yargsHelper.command(commandName).description(command.description, Assigns.COMMAND).runner(command.runner(config));

      command.aliases?.forEach(yargsHelper.alias.bind(yargsHelper));
      Object.entries(command.options || {}).forEach(([optionName, option]) =>
        yargsHelper
          .option(option.type, optionName)
          .alias(option.alias, Assigns.OPTIONS)
          .description(option.description, Assigns.OPTIONS)
          .choices(option.choices)
          .isArray(option.isArray)
          .default(option.default)
      );

      positionalCommands.forEach(positionalCommandName => {
        const matches = positionalCommandName.match(POSITIONAL_OPTION_REGEX);

        if (!matches) {
          throw new Error(`Invalid positional command "${positionalCommandName}"`);
        }

        const [, openBracket, argument, type = ":string", defaultValue, closeBracket] = matches;
        const clearType = type.slice(1) as CommandPositionalOptions["type"];
        let clearDefaultValue = defaultValue?.slice(1) as CommandPositionalOptions["default"];
        const validTypes: CommandPositionalOptions["type"][] = ["string", "boolean", "number"];

        if ((openBracket === "<" && closeBracket === "]") || (openBracket === "[" && closeBracket === ">")) {
          throw new Error(`Invalid positional command "${positionalCommandName}"`);
        }

        if (!validTypes.includes(clearType)) {
          throw new Error(`Invalid positional command type "${clearType}"`);
        }

        if (defaultValue) {
          if (clearType === "boolean") {
            if (!clearDefaultValue.toString().match(VALIDATE_BOOLEAN_REGEX))
              throw new Error(`Invalid default value "${clearDefaultValue}" for positional command "${positionalCommandName}"`);
            clearDefaultValue = clearDefaultValue === "true";
          }

          if (clearType === "number") {
            if (!clearDefaultValue.toString().match(VALIDATE_NUMBER_REGEX))
              throw new Error(`Invalid default value "${clearDefaultValue}" for positional command "${positionalCommandName}"`);
            clearDefaultValue = +clearDefaultValue;
          }
        }

        // @ts-ignore
        const positional: CommandPositionalOptions = {
          type: clearType,
          default: clearDefaultValue,
          ...(command.positional?.[argument] || {})
        };

        yargsHelper.positional(argument, positional);
      });

      yargsHelper.endOption();
      yargsHelper.endCommand();
    }
  }
};
