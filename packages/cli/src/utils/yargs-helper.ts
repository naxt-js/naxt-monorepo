import yargs from "yargs";
import { Assigns, Command, CommandPositionalOptions, CliOption, RunnerFunction, RunnerFunctions } from "@naxt/types";

interface ActiveCommand extends Omit<Command, "runner"> {
  runner: RunnerFunctions;
}

export class YargsHelper {
  /* commands */
  activeCommand: ActiveCommand | null = null;
  commands: Record<string, Omit<ActiveCommand, "name">> = {};

  commandOptions: Record<string, Record<string, CliOption>> = {};
  commandNames = new Set<string>();
  commandPositional: Record<string, Record<string, CommandPositionalOptions>> = {};

  /* options */
  activeOption: CliOption | null = null;
  options: Record<string, Omit<CliOption, "name">> = {};

  /* others */
  private readonly yargs = yargs();

  // region Option-Command Methods

  description(description: string, assignTo: Assigns) {
    if (this.activeCommand && assignTo === Assigns.COMMAND) {
      this.activeCommand.description = description;
    }

    if (this.activeOption && assignTo === Assigns.OPTIONS) {
      this.activeOption.description = description;
    }

    return this;
  }

  alias(alias: string, assignTo: Assigns) {
    if (this.activeCommand && [Assigns.COMMAND].includes(assignTo)) {
      this.activeCommand.aliases ||= [];

      if (this.activeCommand.aliases.includes(alias)) {
        throw new Error(`Alias ${alias} already exists for command ${this.activeCommand.name}`);
      }

      this.activeCommand.aliases.push(alias);
    }

    if (this.activeOption && [Assigns.OPTIONS].includes(assignTo)) {
      if (this.activeOption.alias) {
        throw new Error(`Alias ${alias} already exists for option ${this.activeOption.name}`);
      }

      this.activeOption.alias = alias;
    }

    return this;
  }

  // endregion

  command(name: string) {
    this.activeCommand && this.endCommand();
    this.activeCommand = { name, description: "", aliases: [], options: {}, runner: [] };
    return this;
  }

  positional(name: string, options: CommandPositionalOptions) {
    if (this.activeCommand) {
      this.commandPositional[this.activeCommand.name] ||= {};
      this.commandPositional[this.activeCommand.name][name] = options;
    }

    return this;
  }

  runner(runner: RunnerFunction) {
    if (this.activeCommand) {
      this.activeCommand.runner.push(runner);
    }

    return this;
  }

  endCommand() {
    if (this.activeCommand) {
      if (this.commandNames.has(this.activeCommand.name)) {
        this.commands[this.activeCommand.name].aliases.push(...(this.activeCommand.aliases || []));
        this.commands[this.activeCommand.name].runner.push(...(this.activeCommand.runner || []));
      } else {
        this.commandNames.add(this.activeCommand.name);
        this.activeCommand.aliases?.forEach(alias => this.commandNames.add(alias));
        this.commands[this.activeCommand.name] = this.activeCommand;
      }

      this.activeCommand = null;
    }

    return this;
  }

  option<T extends CliOption["type"]>(type: T, option: string) {
    this.activeOption && this.endOption();
    this.activeOption = { type, name: option };
    return this;
  }

  // region Option Methods

  default(defaultValue: CliOption["default"]) {
    if (this.activeOption) {
      this.activeOption.default = defaultValue;
    }

    return this;
  }

  choices(choices: string[]) {
    if (this.activeOption) {
      this.activeOption.choices = choices;
    }

    return this;
  }

  isArray(isArray: boolean) {
    if (this.activeOption) {
      this.activeOption.isArray = isArray;
    }

    return this;
  }

  // endregion

  endOption() {
    if (this.activeCommand && this.activeOption) {
      this.commandOptions[this.activeCommand.name] ||= {};

      if (this.commandOptions[this.activeCommand.name][this.activeOption.name]) {
        throw new Error(`Option ${this.activeOption.name} already exists for command ${this.activeCommand.name}`);
      }

      this.commandOptions[this.activeCommand.name][this.activeOption.name] = this.activeOption;
    } else if (this.activeOption) {
      if (this.options[this.activeOption.name]) {
        throw new Error(`Option ${this.activeOption.name} already exists`);
      }

      this.options[this.activeOption.name] = this.activeOption;
    }

    this.activeOption = null;
    return this;
  }

  build() {
    this.activeOption && this.endOption();
    this.activeCommand && this.endCommand();

    const commandEntries = Object.entries(this.commands);

    for (const [commandName, command] of commandEntries) {
      this.yargs.command(
        commandName,
        command.description || "",
        yargs => {
          this.buildOption(yargs.version(false), this.commandOptions[commandName] || {});
          const positional = this.commandPositional[commandName] || {};

          for (const [name, options] of Object.entries(positional)) {
            yargs.positional(name, options);
          }
        },
        async args => {
          try {
            for (const runner of command.runner) {
              await runner(args);
            }
          } catch (e) {
            console.error(e);
            process.exit(1);
          }
        }
      );
    }

    this.buildOption(this.yargs, this.options);

    return this.yargs;
  }

  private buildOption(yargs: yargs.Argv, options: Record<string, Omit<CliOption, "name">>) {
    const optionEntries = Object.entries(options);

    for (const [optionName, option] of optionEntries) {
      yargs.option(optionName, {
        type: option.type,
        description: option.description,
        choices: option.choices,
        alias: option.alias,
        array: option.isArray,
        default: option.default
      });
    }
  }
}
