import { rollup } from "@naxt/rollup";
import { Path } from "@naxt/utils";
import { AppConfig } from "@naxt/types";
import _ from "lodash";

import { defaultConfigApp } from "./default-config-app";

export const resolveConfig = async (root: string): Promise<AppConfig> => {
  let configFile: Path;
  const defaultAppConfig = defaultConfigApp() as AppConfig;
  defaultAppConfig.rootDir = root;

  try {
    configFile = Path.from(root).resolve.setPath("naxt.config").findByExtensions(["js", "ts", "json"]);
  } catch {
    return defaultAppConfig;
  }

  const generatedData = await rollup({ file: configFile, config: defaultAppConfig });
  const configJs = generatedData["naxt.config.js"];

  const exports = {};
  const module = { exports };
  eval(configJs);
  const config = module.exports;

  return _.merge(defaultAppConfig, config);
};
