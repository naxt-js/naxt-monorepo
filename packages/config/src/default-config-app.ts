import { AppConfig } from "@naxt/types";
import { PartialDeep } from "type-fest";

export const defaultConfigApp = (): PartialDeep<AppConfig> => ({});
