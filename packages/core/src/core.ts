import { AppConfig } from "@naxt/types";
import { PartialDeep } from "type-fest";

export const defineConfig = (config: PartialDeep<AppConfig>) => config;
