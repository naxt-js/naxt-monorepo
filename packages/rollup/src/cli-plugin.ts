import { CliPluginHandler } from "@naxt/types";
import { generateHash, isDev, Path } from "@naxt/utils";
import { InternalAppConfig } from "@naxt/types";

import { RollupOptionsBuilder } from "./core/rollup-options-builder";
import { worker } from "./core";
import { resolvePlugins } from "./plugins";

export const naxtCliPlugin: CliPluginHandler = () => ({
  name: "naxt-cli-rollup-plugin",
  commands: [
    {
      name: "build [entry]",
      description: "Build command",
      positional: {
        entry: { description: "Entry file" }
      },
      runner(config: InternalAppConfig) {
        config.isBuild = !isDev;

        return async args => {
          const rollupOptionsBuilder = new RollupOptionsBuilder();

          const paths = (args.entry as string) || config.entry || "src/index.[jt]s";
          const entries = (Array.isArray(paths) ? paths : [paths]).flatMap(path =>
            Path.from(path, config.rootDir).resolve.glob({ asPath: true })
          );

          if (entries.length === 0) {
            throw new Error(`No entry file found for ${args.entry}`);
          }

          if (entries.length > 1) {
            throw new Error(`Multiple entry files found for ${args.entry}`);
          }

          rollupOptionsBuilder
            .input(entries)
            .output({
              format: "esm",
              dir: config.build?.dir || "build",
              entryFileNames(chunk) {
                return `${chunk.name}.${generateHash(chunk.name)}.js`;
              }
            })
            .addPlugins(await resolvePlugins(config));

          await worker(rollupOptionsBuilder, config);
        };
      }
    }
  ]
});
