import { OutputOptions } from "rollup";
import { RollupOptions, RollupPlugin, RollupPlugins } from "@naxt/types";
import { Path } from "@naxt/utils";

export class RollupOptionsBuilder {
  private readonly rollupOptions: RollupOptions;

  constructor() {
    this.rollupOptions = {};
  }

  input(input: RollupOptions["input"] | Path | Path[]) {
    this.rollupOptions.input =
      input instanceof Path
        ? input.fullPath
        : Array.isArray(input)
        ? input.map(path => (path instanceof Path ? path.fullPath : path))
        : input;

    return this;
  }

  onwarn(onwarn: RollupOptions["onwarn"] = () => {}) {
    this.rollupOptions.onwarn = onwarn;
    return this;
  }

  output(output: OutputOptions) {
    this.rollupOptions.output ||= [];
    (this.rollupOptions.output as OutputOptions[]).push(output);
    return this;
  }

  addPlugin(plugin: RollupPlugin) {
    this.rollupOptions.plugins ||= [];
    Array.isArray(this.rollupOptions.plugins) && this.rollupOptions.plugins.push(plugin);
    return this;
  }

  addPlugins(plugins: RollupPlugins) {
    this.rollupOptions.plugins = plugins;
    return this;
  }

  build() {
    return this.rollupOptions;
  }
}
