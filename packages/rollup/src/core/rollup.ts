import { rollup as mainRollupExecutor } from "rollup";
import { TextDecoder } from "util";
import { filesize } from "filesize";

import { isDev, Path, StringBuilder } from "@naxt/utils";
import { AppConfig, InternalAppConfig } from "@naxt/types";
import { Logger } from "@naxt/logger";

import { RollupOptions } from "../types/rollup-options";
import { RollupOptionsBuilder } from "./rollup-options-builder";
import { name, version } from "../../package.json";
import { RollupLogLine } from "../types/rollup-log-line";
import { getLogs } from "../utils/Logger";
import { typescriptPlugin } from "../plugins/typescript-plugin";

const logger = new Logger("rollup", "$message$");

interface WorkerOptions {
  saveToFile?: boolean;
  isLogging?: boolean;
}

export const rollup = ({ file, config, plugins = [] }: RollupOptions) => {
  if (file.extension.isSameTo("ts") && !plugins.some(p => p.name === "typescript")) {
    plugins.push(typescriptPlugin(config));
  }

  const rollupOptionsBuilder = new RollupOptionsBuilder()
    .input(file.fullPath)
    .onwarn()
    .output({ format: "commonjs", chunkFileNames: () => "bundle" })
    .addPlugins(plugins);

  return worker(rollupOptionsBuilder, config, { isLogging: false, saveToFile: false });
};

export async function worker(rollupOptionsBuilder: RollupOptionsBuilder, config: InternalAppConfig, options?: WorkerOptions): Promise<void>;
export async function worker(
  rollupOptionsBuilder: RollupOptionsBuilder,
  config: AppConfig,
  options?: WorkerOptions
): Promise<Record<string, string>>;
export async function worker(
  rollupOptionsBuilder: RollupOptionsBuilder,
  config: AppConfig,
  { saveToFile = true, isLogging = true }: WorkerOptions = {}
): Promise<void | Record<string, string>> {
  logger.setIsLogging(isLogging);
  const stringBuilder = new StringBuilder().setLogger(logger).clear();
  const env = isDev ? "development" : "production";

  stringBuilder.cyan(`${name} v${version}`).spaces().green(`building for ${env}...`).log();

  const rollupOptions = rollupOptionsBuilder.build();

  const bundle = await mainRollupExecutor(rollupOptions);
  const outputOptions = Array.isArray(rollupOptions.output) ? rollupOptions.output : [rollupOptions.output];
  const out: Record<string, string> = {};
  const lines: RollupLogLine[] = [];
  let modulesCount = 0;

  for (const outputOption of outputOptions) {
    const { output } = await bundle.generate(outputOption);

    modulesCount += output.length;
    for (const chunk of output) {
      const data =
        chunk.type === "chunk"
          ? chunk.code
          : typeof chunk.source === "string"
          ? Path.from(chunk.source).source.readBytes().toString("hex")
          : new TextDecoder().decode(chunk.source);

      if (saveToFile) {
        const filePath = Path.from(config.rootDir)
          .source.mkdir()
          .duplicateTo(outputOption.dir || config.rootDir)
          .duplicateTo(chunk.fileName);

        if (chunk.type === "chunk") {
          filePath.source.mkdir(filePath.dirname);
          filePath.source.write(data);

          const relativeFilePath = filePath.relativeTo(config.rootDir, true);
          const [basePath, ...otherPaths] = relativeFilePath.split("/");
          const mainFile = otherPaths.join("/");

          lines.push({ basePath, mainFile, size: data.length });
        }
      } else {
        out[chunk.fileName] = data;
      }
    }
  }

  if (isLogging) {
    const logs = getLogs();

    Object.entries(logs).forEach(([file, { size }]) => {
      const filePath = Path.from(file);
      const relativeFilePath = filePath.relativeTo(config.rootDir, true);
      const [basePath, ...otherPaths] = relativeFilePath.split("/");
      lines.push({ basePath, mainFile: otherPaths.join("/"), size });
    });

    const spaceBetweenSizeAndName = 3;
    const printedLines = lines
      .map(({ size, ...line }) => ({
        ...line,
        filesize: filesize(size).toString(),
        maxLength: line.basePath.length + line.mainFile.length + spaceBetweenSizeAndName
      }))
      .sort((a, b) => a.maxLength - b.maxLength);
    const lastLine = printedLines.at(-1);
    const valuesMaxLength = lastLine ? lastLine.maxLength : 0;
    const maxLength = Math.min(valuesMaxLength, process.stdout.columns - spaceBetweenSizeAndName - (lastLine?.filesize.length || 0));
    const modulesAdded = "modules added";
    stringBuilder
      .spaces(Math.max(valuesMaxLength - modulesAdded.length - 5, 0))
      .tick()
      .spaces(2)
      .append(modulesCount)
      .spaces()
      .append(modulesAdded)
      .log();
    printedLines.forEach(({ basePath, mainFile, filesize }) => {
      const pathLength = basePath.length + mainFile.length + 1;

      stringBuilder
        .spaces(Math.max(maxLength - pathLength, 0))
        .append(mainFile.length > maxLength - 3 ? "..." : `${basePath}/`.slice(Math.min(mainFile.length - maxLength, 0)))
        .cyan(mainFile.length > maxLength - 3 ? mainFile.slice(-maxLength + 3) : mainFile)
        .spaces(spaceBetweenSizeAndName)
        .append(filesize)
        .log();
    });
  }

  logger.setIsLogging(true);

  if (!saveToFile) {
    return out;
  }
}
