import { OutputOptions } from "rollup";

import { generateHash, Path } from "@naxt/utils";

import { MEDIA_EXTENSIONS } from "../utils/extensions";
import { addFileLog } from "../utils/Logger";
import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { __NAXT_MEDIA_ASSET__ } from "../utils/constants";

interface Asset {
  name: string;
  extension: string;
  hash: string;
}

export const assetsPlugin: RollupPluginHandler = config => {
  const assetsMap = new Map<Path, Asset>();
  const outputOptions: OutputOptions[] = [];
  const defaultAssetFileName = `assets/[name].[hash].[ext]`;

  return {
    name: "naxt-media-plugin",

    load(source) {
      const path = Path.from(source);

      if (path.extension.isSameOneOf(MEDIA_EXTENSIONS)) {
        const hash = generateHash(path.importPath);
        const extension = path.extension.getExtension();
        const name = path.extension.removeExtension().basename;
        path.extension.setExtension(extension);

        assetsMap.set(path, { name, hash, extension });
        return `export default "${__NAXT_MEDIA_ASSET__}_${hash}";`;
      }
    },

    outputOptions(_outputOptions) {
      outputOptions.push(_outputOptions);
    },

    renderChunk(code, chunk) {
      const rootParents = chunk.getRootParents();

      rootParents.forEach(parent => {
        assetsMap.forEach(({ name, hash, extension }, assetPath) => {
          if (parent.modules[assetPath.importPath]) {
            outputOptions.forEach(outputOption => {
              const assetFilename =
                typeof outputOption.assetFileNames === "string"
                  ? outputOption.assetFileNames
                  : outputOption.assetFileNames?.({ type: "asset", source: code, name: `${name}.${extension}` }) || defaultAssetFileName;

              const fileName = assetFilename
                .replace("[name]", name)
                .replace("[hash]", hash)
                .replace("[ext]", extension)
                .replace("[extname]", `.${extension}`);

              code = code.replace(`"${__NAXT_MEDIA_ASSET__}_${hash}"`, `"/${fileName}"`);

              const assetOutputPath = Path.from(outputOption.dir, config.rootDir).duplicateTo(fileName);
              assetPath.copyTo(assetOutputPath);

              parent.importedAssets.add(fileName);
              this.emitFile({ type: "asset", fileName, name: assetOutputPath.basename });
              addFileLog({ file: assetOutputPath.fullPath, size: assetOutputPath.stats.size });
            });
          }
        });
      });

      return code;
    }
  };
};
