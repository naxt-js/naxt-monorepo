import { Path } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { ChunkMetadata } from "../types/chunk-metadata";
import { Graph } from "../utils/graph";

declare module "rollup" {
  interface RenderedChunk extends ChunkMetadata {
    getRootParents(): RenderedChunk[];
  }
}

export const chunkMetadataPlugin: RollupPluginHandler = () => {
  const moduleGraph = new Graph();

  return {
    name: "chunk-metadata-plugin",

    resolveId(source, importer) {
      if (importer) {
        const importerPath = Path.from(importer);

        moduleGraph.addChildren(importerPath.fullPath, importerPath.duplicateTo(source).resolve.findFile().fullPath);
      } else {
        moduleGraph.createModule(source, true);
      }
    },

    renderChunk(code, chunk) {
      chunk.importedAssets = new Set();
      chunk.importedCss = new Set();

      moduleGraph.setChunk(chunk.facadeModuleId, chunk);
      chunk.getRootParents = () => moduleGraph.getRootParents(chunk.facadeModuleId).map(m => m.chunk);

      return code;
    }
  };
};
