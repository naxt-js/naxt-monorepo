import { RollupOptions } from "@naxt/types";
import { Path } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";

export const cleanBuildDir: RollupPluginHandler = config => ({
  name: "clean-build-dir",

  options(options: RollupOptions) {
    const outputDirs = Array.isArray(options.output) ? options.output.map(o => o.dir) : [options.output?.dir];

    outputDirs.filter(Boolean).forEach(dir => {
      Path.from(dir, config.rootDir).source.rmdir();
    });
  }
});
