import { OutputOptions } from "rollup";
import Axios from "axios";
import MagicString from "magic-string";

import { CSS_IMPORT_REGEX, Path, URL_REGEX } from "@naxt/utils";

import { ALL_CSS_EXTENSIONS, LESS_EXTENSIONS, SASS_EXTENSIONS, STYLUS_EXTENSIONS } from "../utils/extensions";
import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { addFileLog } from "../utils/Logger";

const compileSass = async (sass: typeof import("sass"), code: string, rootPath: Path) => {
  const { css } = await sass.compileStringAsync(code);
  const ms = new MagicString(css);

  const matchedImports = css.matchAll(new RegExp(CSS_IMPORT_REGEX, "g"));

  for (const { 0: fullData, 1: path, index } of matchedImports) {
    let updatedCss = "";

    if (path.match(URL_REGEX)) {
      const { data } = await Axios.get<string>(path);
      updatedCss = data;
    } else {
      const filePath = rootPath.duplicateTo(path);
      const content = filePath.source.read();
      updatedCss = await compileSass(sass, content, filePath);
    }

    ms.overwrite(index, index + fullData.length, updatedCss);
  }

  return ms.toString();
};

export const cssPlugin: RollupPluginHandler = config => {
  const sourceCode: Record<string, { position: Record<string, number>; fragments: string[] }> = {};
  const outputOptions: OutputOptions[] = [];

  return {
    name: "naxt-css-plugin",

    async transform(code, source) {
      const path = Path.from(source);

      if (!path.extension.isSameOneOf(ALL_CSS_EXTENSIONS)) return;
      sourceCode[source] = { position: {}, fragments: [] };

      const postcssPlugins = [];
      const isModule = path.extension.isSameOneOf(ALL_CSS_EXTENSIONS.map(ext => `.module.${ext}`));
      let json: Record<string, string> = {};

      if (isModule) {
        postcssPlugins.push((await import("postcss-modules")).default({ getJSON: (_, _json) => (json = _json) }));
      }

      if (path.extension.isSameOneOf(SASS_EXTENSIONS)) {
        const sass = await import("sass");
        code = await compileSass(sass, code, path);
      }

      if (path.extension.isSameOneOf(LESS_EXTENSIONS)) {
        const { default: less } = await import("less");
        const { css } = await less.render(code, { filename: source });
        code = css;
      }

      if (path.extension.isSameOneOf(STYLUS_EXTENSIONS)) {
        const stylus = await import("stylus");
        code = stylus.render(code);
      }

      const { default: postcss } = await import("postcss");
      const { css } = await postcss(...postcssPlugins).process(code, { from: source, to: source });

      if (source in sourceCode[source].position) {
        sourceCode[source].fragments[sourceCode[source].position[source]] = css;
      } else {
        sourceCode[source].position[source] = sourceCode[source].fragments.length;
        sourceCode[source].fragments.push(css);
      }

      if (isModule) {
        const jsCssMap = `export default ${JSON.stringify(json)};`;
        const ms = new MagicString(jsCssMap);
        return { code: ms.toString(), map: ms.generateMap({ hires: true }) };
      }

      return { code: ``, moduleSideEffects: true };
    },

    outputOptions(options) {
      outputOptions.push(options);
    },

    renderChunk(code, chunk) {
      const rootParents = chunk.getRootParents();
      const cssCode = Object.entries(sourceCode)
        .filter(([module]) => chunk.modules[module])
        .flatMap(([file, data]) => `/*** ${file} ***/\n${data.fragments.join("\n")}`)
        .join("\n");

      for (const outputOption of outputOptions) {
        const basePath = Path.from(outputOption.dir, config.rootDir);

        rootParents.forEach(parent => {
          const parentNamePath = Path.from(parent.fileName);
          parentNamePath.extension.removeExtension().extension.setExtension("css");
          const filename = parentNamePath.basename;
          const fileOutputPath = Path.from(filename, basePath);
          fileOutputPath.source.write(cssCode);
          parent.importedCss.add(filename);

          this.emitFile({ type: "asset", fileName: filename, name: filename });
          addFileLog({ file: fileOutputPath.fullPath, size: fileOutputPath.stats.size });
        });
      }

      return code;
    }
  };
};
