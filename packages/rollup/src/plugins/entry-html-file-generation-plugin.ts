import { OutputOptions } from "rollup";
import { PackageJson } from "type-fest";

import { Path, StringBuilder } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { addFileLog } from "../utils/Logger";

export const entryHtmlFileGenerationPlugin: RollupPluginHandler = config => {
  const outputOptions: OutputOptions[] = [];

  return {
    name: "naxt-entry-html-file-generation-plugin",

    outputOptions(_outputOptions) {
      outputOptions.push(_outputOptions);
    },

    renderChunk: {
      order: "post",
      async handler(code, { importedAssets, importedCss, fileName }) {
        const rootPackageJson: PackageJson = await import(Path.from("package.json", config.rootDir).fullPath);

        outputOptions.forEach(output => {
          const outputFilePath = Path.from(output.dir, config.rootDir).duplicateTo(fileName);
          outputFilePath.extension.removeExtension(2).extension.setExtension("html");

          const html = new StringBuilder()
            .separator("\n")
            .append(`<!DOCTYPE html>`)
            .append(`<html lang="en">`)
            .append(`  <head>`)
            .append(`    <meta charset="UTF-8" />`)
            .append(`    <meta http-equiv="X-UA-Compatible" content="IE=edge" />`)
            .append(`    <meta name="viewport" content="width=device-width, initial-scale=1.0" />`)
            .append(`    <title>${config.build?.title || rootPackageJson.name}</title>`)

            .append(``)
            .appendGroupIf(asset => `    <link rel="prefetch" href="/${asset}" />`, importedAssets, importedAssets.size > 0)

            .append(``)
            .appendGroupIf(cssFile => `    <link rel="stylesheet" href="/${cssFile}" />`, importedCss, importedCss.size > 0)

            .append(`  </head>`)
            .append(`  <body>`)
            .append(`    <div id="root"></div>`)
            .append(``)
            .append(`    <script src="/${fileName}"></script>`)
            .append(`  </body>`)
            .append(`</html>`)
            .build();

          outputFilePath.source.write(html);
          this.emitFile({ type: "asset", fileName: outputFilePath.basename, name: outputFilePath.basename });
          addFileLog({ file: outputFilePath.fullPath, size: outputFilePath.stats.size });
        });

        return code;
      }
    }
  };
};
