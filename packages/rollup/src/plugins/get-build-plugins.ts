import { RollupPlugin, InternalAppConfig } from "@naxt/types";

import { terserPlugin } from "./terser-plugin";

export const getBuildPlugins = (config: InternalAppConfig) => {
  const prePlugins: RollupPlugin[] = [];
  const postPlugins: RollupPlugin[] = [];

  postPlugins.push(terserPlugin(config));

  return { pre: prePlugins, post: postPlugins };
};
