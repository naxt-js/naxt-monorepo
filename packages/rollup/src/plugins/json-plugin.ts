import MagicString from "magic-string";
import { dataToEsm } from "@rollup/pluginutils";

import { isDev, Path, stripBOM } from "@naxt/utils";
import { JsonOptions } from "@naxt/types";

import { JSON_EXTENSIONS } from "../utils/extensions";
import { RollupPluginHandler } from "../types/rollup-plugin-handler";

export const jsonPlugin: RollupPluginHandler = config => {
  const { isBuild, build: { json: jsonOptions = {} as JsonOptions } = {} } = config;
  const indent = typeof jsonOptions.indent === "number" ? " ".repeat(jsonOptions.indent) : jsonOptions.indent || "  ";

  return {
    name: "naxt-json-plugin",

    resolveId(source) {
      const path = Path.from(source);

      if (path.extension.isSameOneOf(JSON_EXTENSIONS)) {
        return source;
      }
    },

    transform(code, source) {
      const path = Path.from(source);
      const json = stripBOM(code);

      if (!path.extension.isSameOneOf(JSON_EXTENSIONS)) return;
      let resultCode: string;

      try {
        const parsedJson = isBuild ? JSON.parse(JSON.stringify(JSON.parse(json))) : JSON.parse(json);

        resultCode = jsonOptions.stringify
          ? `export default JSON.parse(${JSON.stringify(parsedJson)})`
          : dataToEsm(parsedJson, { preferConst: true, namedExports: jsonOptions.namedExports, objectShorthand: !isDev, indent });
      } catch (e) {
        const errorMessageList = /\d+/.exec(e.message);
        const position = errorMessageList && parseInt(errorMessageList[0], 10);
        const msg = position ? `, invalid JSON syntax found at line ${position}` : `.`;
        this.error(`Failed to parse JSON file${msg}`, e.idx);
      }

      const ms = new MagicString(resultCode);
      return { code: ms.toString(), map: ms.generateMap({ hires: true }) };
    }
  };
};
