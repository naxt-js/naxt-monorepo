import { Path } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";

export const pathResolver: RollupPluginHandler = () => {
  return {
    name: "resolve-path",

    resolveId(source, importer) {
      if (source.startsWith(".")) {
        return Path.from(importer).duplicateTo(source).resolve.findFile().importPath;
      }
    }
  };
};
