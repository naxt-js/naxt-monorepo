import { Options as PrettierOptions } from "prettier";
import { PackageJson } from "type-fest";

import { Path } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { PRETTIER_CONFIG_FILE_LIST } from "../utils/extensions";

export const prettierPlugin: RollupPluginHandler = config => {
  const fileConfigsPromises = Path.resolveFiles(config.rootDir, PRETTIER_CONFIG_FILE_LIST)
    .filter(path => path.exists)
    .map(path => path.source.readAndParse<PrettierOptions>());
  let format: (source: string, options?: PrettierOptions) => string = source => source;

  let prettierFileConfigs: PrettierOptions[] = [];
  let prettierConfig: PrettierOptions = {};

  const pkgPrettierConfig = (Path.from("package.json", config.rootDir).source.readJson<PackageJson>().prettier as PrettierOptions) || null;
  const configPrettierConfig = config.build?.prettier || null;

  return {
    name: "naxt-prettier-plugin",

    async buildStart() {
      format = (await import("prettier")).format;
      prettierFileConfigs = await Promise.all(fileConfigsPromises);
      prettierFileConfigs.push(pkgPrettierConfig, configPrettierConfig);
      prettierFileConfigs = prettierFileConfigs.filter(Boolean);

      if (prettierFileConfigs.length > 1) {
        this.warn("Prettier config files found more than one. Using the first one.");
      }

      prettierConfig = prettierFileConfigs[0] || {};
    },

    async transform(code) {
      return format(code, { ...prettierConfig, parser: "babel" });
    }
  };
};
