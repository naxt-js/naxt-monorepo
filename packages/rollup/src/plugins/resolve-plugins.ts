import { RollupPlugins, InternalAppConfig } from "@naxt/types";
import { isDev } from "@naxt/utils";

import { filterPluginsByEnforce } from "../utils/filter-plugins-by-enforce";

// region Internal Plugins
import { chunkMetadataPlugin } from "./chunk-metadata-plugin";
import { cleanBuildDir } from "./clean-build-dir";
import { pathResolver } from "./path-resolver";
import { typescriptPlugin } from "./typescript-plugin";
import { jsonPlugin } from "./json-plugin";
import { assetsPlugin } from "./assets-plugin";
import { cssPlugin } from "./css-plugin";
import { prettierPlugin } from "./prettier-plugin";
import { entryHtmlFileGenerationPlugin } from "./entry-html-file-generation-plugin";
// endregion

export const resolvePlugins = async (config: InternalAppConfig) => {
  const plugins: RollupPlugins = [];
  const { isBuild } = config;

  const enforcePlugins = filterPluginsByEnforce(config.plugins || []);
  const internalBuildPlugins = isBuild ? (await import("./get-build-plugins")).getBuildPlugins(config) : { pre: [], post: [] };
  const userBuildPlugins = isBuild ? filterPluginsByEnforce(config.build?.plugins || []) : { pre: [], normal: [], post: [] };

  plugins.push(chunkMetadataPlugin(config));
  plugins.push(cleanBuildDir(config));
  plugins.push(pathResolver(config));
  plugins.push(typescriptPlugin(config));
  plugins.push(jsonPlugin(config));
  plugins.push(assetsPlugin(config));
  plugins.push(cssPlugin(config));

  plugins.push(...enforcePlugins.pre);

  plugins.push(...enforcePlugins.normal);

  plugins.push(...internalBuildPlugins.pre);
  plugins.push(...userBuildPlugins.pre);

  plugins.push(...enforcePlugins.post);

  plugins.push(...userBuildPlugins.normal);

  plugins.push(...userBuildPlugins.post);
  plugins.push(...internalBuildPlugins.post);

  isDev && plugins.push(prettierPlugin(config));
  plugins.push(entryHtmlFileGenerationPlugin(config));

  return plugins;
};
