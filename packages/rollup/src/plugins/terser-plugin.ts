import MagicString from "magic-string";

import { isDev } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";

export const terserPlugin: RollupPluginHandler = config => {
  const { build: { minify = {} } = {} } = config;

  const minifyOptions = typeof minify === "boolean" ? {} : minify;

  return {
    name: "naxt-terser-plugin",

    async renderChunk(code, chunk, outputOptions) {
      const terser = await import("terser");

      const { code: minifiedCode } = await terser.minify(code, {
        safari10: true,
        ...minifyOptions,
        sourceMap: !!outputOptions.sourcemap,
        module: outputOptions.format.startsWith("es"),
        toplevel: outputOptions.format === "cjs",
        format: {
          comments: isDev ? "all" : false,
          preamble: isDev ? "/* naxt */" : "",
          ...(minifyOptions.format || {})
        }
      });

      const ms = new MagicString(minifiedCode);
      return { code: ms.toString(), map: ms.generateMap({ hires: true }) };
    }
  };
};
