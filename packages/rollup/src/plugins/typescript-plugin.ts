import { ModuleKind, ScriptTarget, transpileModule } from "typescript";
import MagicString from "magic-string";
import { TsConfigJson } from "type-fest";

import { Path } from "@naxt/utils";

import { RollupPluginHandler } from "../types/rollup-plugin-handler";
import { TS_EXTENSIONS } from "../utils/extensions";
import { AppConfig } from "@naxt/types";

export const typescriptPlugin: RollupPluginHandler<AppConfig> = config => {
  const rootTsConfigFile = Path.from("tsconfig.json", config.rootDir);
  const tsConfig = rootTsConfigFile.exists ? rootTsConfigFile.source.readJson<TsConfigJson>() : {};

  let warningMessage: string = "";

  if (!rootTsConfigFile.exists) {
    warningMessage = "TSConfig not found. Will be used default values";
  } else if (tsConfig.compilerOptions?.module?.toLowerCase() !== "esnext" || tsConfig.compilerOptions?.target?.toLowerCase() !== "esnext") {
    warningMessage = "tsconfig.compilerOptions.(module|target) is not ESNext. It will be changed automatically";
  }

  tsConfig.compilerOptions ||= {};
  tsConfig.compilerOptions.module = "ESNext";
  tsConfig.compilerOptions.target = "ESNext";

  return {
    name: "typescript",

    async transform(code, source) {
      const path = Path.from(source);

      if (!path.extension.isSameOneOf(TS_EXTENSIONS)) {
        return;
      }

      if (warningMessage) {
        this.warn(warningMessage);
      }

      const compiledCode = transpileModule(code, {
        compilerOptions: {
          target: ScriptTarget.ESNext,
          module: ModuleKind.ESNext
        }
      });
      const ms = new MagicString(compiledCode.outputText);
      return { code: ms.toString(), map: ms.generateMap({ hires: true }) };
    }
  };
};
