import { RenderedChunk } from "rollup";

export interface GraphInterface {
  parents: Set<GraphInterface>;
  children: Set<GraphInterface>;

  chunk: null | RenderedChunk;

  file: string;
  isRoot: boolean;
}
