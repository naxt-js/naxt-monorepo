export interface RollupLogLine {
  basePath: string;
  mainFile: string;
  size: number;
}
