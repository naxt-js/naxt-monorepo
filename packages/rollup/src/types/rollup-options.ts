import { AppConfig, RollupPlugins } from "@naxt/types";
import { Path } from "@naxt/utils";

export interface RollupOptions {
  file: Path;
  config: AppConfig;
  plugins?: RollupPlugins;
}
