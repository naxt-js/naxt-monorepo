import { AppConfig, InternalAppConfig, RollupPlugin } from "@naxt/types";

export type RollupPluginHandler<T extends AppConfig = InternalAppConfig> = (config: T) => RollupPlugin;
