interface Logs {
  /**
   * The name of the asset/chunk file.
   */
  file: string;

  /**
   * The size of the asset/chunk file in bytes.
   */
  size: number;
}

class Logger {
  constructor(private hashMap = new Map<string, Omit<Logs, "file">>()) {}

  addFileLog(log: Logs) {
    const { file, ...rest } = log;
    this.hashMap.set(file, rest);
  }

  getLogs() {
    return Object.fromEntries(this.hashMap.entries());
  }
}

const logger = new Logger();
export const addFileLog: Logger["addFileLog"] = logger.addFileLog.bind(logger);
export const getLogs: Logger["getLogs"] = logger.getLogs.bind(logger);
