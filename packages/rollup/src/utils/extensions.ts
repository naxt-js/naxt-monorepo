export const MEDIA_EXTENSIONS = ["jpg", "jpeg", "png", "svg", "gif", "webp"];

export const JS_EXTENSIONS = ["js", "jsx", "mjs", "cjs"];
export const TS_EXTENSIONS = ["ts"];
export const JSX_EXTENSIONS = ["jsx", "tsx"];
export const VUE_EXTENSIONS = ["vue"];
export const JSON_EXTENSIONS = ["json", "json5"];
export const XML_EXTENSIONS = ["html", "htm", "xhtml", "xht", "shtml", "shtm", "xsl", "xslt", "xml"];

export const SASS_EXTENSIONS = ["scss", "sass"];
export const LESS_EXTENSIONS = ["less"];
export const STYLUS_EXTENSIONS = ["styl", "stylus"];
export const CSS_EXTENSIONS = ["css", "pcss", "postcss"];
export const ALL_CSS_EXTENSIONS = [...CSS_EXTENSIONS, ...SASS_EXTENSIONS, ...LESS_EXTENSIONS, ...STYLUS_EXTENSIONS];

export const PRETTIER_CONFIG_FILE_LIST = [
  ".prettierrc",
  ".prettierrc.json",
  ".prettierrc.js",
  ".prettierrc.yaml",
  ".prettierrc.yml",
  ".prettierrc.toml",
  ".prettierrc.cjs",
  "prettier.config.js",
  "prettier.config.cjs",
  "prettier.config.mjs",
  "prettier.config.ts",
  "prettier.config.json",
  "prettier.config.yaml",
  "prettier.config.yml",
  "prettier.config.toml"
];
