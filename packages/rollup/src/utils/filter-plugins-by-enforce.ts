import { RollupPlugins } from "@naxt/types";

export const filterPluginsByEnforce = (plugins?: RollupPlugins) => ({
  pre: plugins.filter(plugin => plugin.enforce === "pre"),
  normal: plugins.filter(plugin => !plugin.enforce),
  post: plugins.filter(plugin => plugin.enforce === "post")
});
