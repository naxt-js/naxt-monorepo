import { RenderedChunk } from "rollup";

import { GraphInterface } from "../types/graph-interface";

export class Graph {
  constructor(
    /**
     * A map of module names to their corresponding file name.
     */
    private moduleMap = new Map<string, GraphInterface>(),
    /**
     * A map of module names to their corresponding graph node.
     */
    private modules = new Map<GraphInterface, GraphInterface>()
  ) {}

  createModule(module: string | GraphInterface, isRoot = false) {
    const moduleNode: GraphInterface =
      typeof module === "string" ? { file: module, isRoot, children: new Set(), parents: new Set(), chunk: null } : module;
    moduleNode.isRoot = isRoot;
    this.moduleMap.set(moduleNode.file, moduleNode);
    this.modules.set(moduleNode, moduleNode);
    return moduleNode;
  }

  getModule(module: string | GraphInterface): GraphInterface {
    return typeof module === "string" ? this.moduleMap.get(module) : this.modules.get(module);
  }

  getOrCreateModule(module: string | GraphInterface) {
    let moduleNode = this.getModule(module);
    if (!moduleNode) {
      moduleNode = this.createModule(module);
    }
    return moduleNode;
  }

  setParents(module: string | GraphInterface, ...parents: (string | GraphInterface)[]) {
    const moduleNode = typeof module === "string" ? this.getOrCreateModule(module) : module;
    for (const parent of parents) {
      const parentNode = typeof parent === "string" ? this.getOrCreateModule(parent) : parent;
      moduleNode.parents.add(parentNode);
      parentNode.children.add(moduleNode);
    }
  }

  setChunk(facadeModuleId: string, chunk: RenderedChunk) {
    const moduleNode = this.getModule(facadeModuleId);
    moduleNode.chunk = chunk;
  }

  addChildren(module: string | GraphInterface, ...children: (string | GraphInterface)[]): void {
    const moduleNode = typeof module === "string" ? this.getOrCreateModule(module) : module;
    for (const child of children) {
      const childNode = typeof child === "string" ? this.getOrCreateModule(child) : child;
      moduleNode.children.add(childNode);
      childNode.parents.add(moduleNode);
    }
  }

  getParents(module: string | GraphInterface) {
    const moduleNode = typeof module === "string" ? this.getModule(module) : module;
    return moduleNode.parents;
  }

  getRootParents(module: string | GraphInterface): GraphInterface[] {
    const moduleNode = typeof module === "string" ? this.getModule(module) : module;
    if (moduleNode.isRoot) {
      return [moduleNode];
    }
    return Array.from(moduleNode.parents).map<GraphInterface>(this.getRootParents.bind(this));
  }
}
