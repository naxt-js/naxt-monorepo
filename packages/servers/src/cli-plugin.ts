import { AppConfig, CliPluginHandler, InternalAppConfig } from "@naxt/types";
import { Path } from "@naxt/utils";

import { CoreServer, DevServer, ProdServer } from "./servers";

export const naxtCliPlugin: CliPluginHandler = () => ({
  name: "naxt-cli-servers-plugin",
  commands: [
    {
      name: "dev",
      aliases: ["serve"],
      options: {
        port: {
          type: "number",
          description: "Port to run the server on",
          default: 7890
        },
        host: {
          type: "string",
          description: "Host to run the server on",
          default: "localhost"
        }
      },
      runner(config: InternalAppConfig) {
        config.isBuild = false;

        return async ({ port, host }) => {
          const server = new CoreServer({ port: port as number, host: host as string });
          const devServer = new DevServer(server);
          devServer.serve(config);
          await server.start();
        };
      }
    },
    {
      name: "preview",
      aliases: ["serve:prod"],
      description: "Run production environment",
      options: {
        port: {
          type: "number",
          description: "Port to run the server on",
          default: 5000
        },
        host: {
          type: "string",
          description: "Host to run the server on",
          default: "localhost"
        }
      },
      runner(config: AppConfig) {
        return async ({ port, host }) => {
          if (!Path.from("build", config.rootDir).exists) {
            throw new Error(`Production build not found. Please run 'naxt build' first.`);
          }

          const server = new CoreServer({ port: port as number, host: host as string });
          const prodServer = new ProdServer(server);
          prodServer.serve(config);
          await server.start();
        };
      }
    }
  ]
});
