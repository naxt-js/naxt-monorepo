import * as http from "http";
import express from "express";
import { createServer } from "net";

import { Logger } from "@naxt/logger";

interface ServerOptions {
  port: number;
  host: string;
}

const isPortBusy = (port: number) =>
  new Promise<boolean>((resolve, reject) => {
    const server = createServer();
    server.unref();
    server.on("error", () => reject(true));
    server.listen(port, () => {
      server.close();
      resolve(false);
    });
  });

export class CoreServer {
  private activeHttpServer: http.Server | null = null;
  private readonly logger = new Logger("CoreServer");
  private readonly app = express();

  constructor(private readonly serverOptions?: ServerOptions) {
    this.serverOptions.port ||= 3000;
    this.serverOptions.host ||= "localhost";
  }

  middleware(requestHandler: express.RequestHandler) {
    this.app.use(requestHandler);
  }

  async start() {
    let isPb = true;
    while (!isPb) {
      isPb = await isPortBusy(this.serverOptions.port);
      isPb && this.serverOptions.port++;
    }

    this.activeHttpServer = this.app.listen(this.serverOptions.port, this.serverOptions.host, () => {
      this.logger.info(`Server started on http://${this.serverOptions.host}:${this.serverOptions.port}`);
    });
  }

  stop() {
    if (this.activeHttpServer) {
      this.activeHttpServer.close();
    }
  }

  async restart() {
    this.stop();
    await this.start();
  }
}
