import { InternalAppConfig } from "@naxt/types";

import { CoreServer } from "./core-server";

export class DevServer {
  constructor(private server: CoreServer) {}

  serve(config: InternalAppConfig) {
    // testing dev server

    const data = { value: 1234 };

    // each second change data.value
    setInterval(() => {
      data.value++;
    }, 1000);

    this.server.middleware((req, res, next) => {
      res.json(data);
    });
  }
}
