import { CoreServer } from "./core-server";
import { AppConfig } from "@naxt/types";
import { Path } from "@naxt/utils";
import express from "express";

export class ProdServer {
  constructor(private readonly coreServer: CoreServer) {}

  serve(config: AppConfig) {
    this.coreServer.middleware(express.static(Path.from("build", config.rootDir).fullPath));
  }
}
