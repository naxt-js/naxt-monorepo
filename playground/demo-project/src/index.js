import { _this } from "./open/close/build/this";
import { name, dependencies } from "../package.json";
import * as data from "./x.json";

import js_png from "./assets/media/javascript.png";
import js_jpg from "./assets/media/javascript.jpg";
import js_gif from "./assets/media/javascript.gif";
import js_svg from "./assets/media/javascript.svg";
import js_webp from "./assets/media/javascript.webp";

import "./assets/styles/index.css";
import "./assets/styles/index.scss";
import "./assets/styles/index.less";
import "./assets/styles/index.styl";

import css from "./assets/styles/index.module.css";
import scss from "./assets/styles/index.module.scss";
import less from "./assets/styles/index.module.less";
import styl from "./assets/styles/index.module.styl";
import { typescriptFile } from "./ts";

const pre = document.createElement("pre");
pre.innerHTML = JSON.stringify(
  {
    _this,
    name,
    dependencies,
    data,
    js_png,
    js_jpg,
    js_gif,
    js_svg,
    js_webp,
    css,
    scss,
    less,
    styl
  },
  null,
  2
);
document.body.appendChild(pre);

[js_png, js_jpg, js_gif, js_svg, js_webp].forEach(imageSrc => {
  const image = new Image();
  image.src = imageSrc;
  document.body.appendChild(image);
});

document.body.appendChild(document.createElement("hr"));
const div = document.createElement("div");
div.textContent = typescriptFile({});
document.body.appendChild(div);
