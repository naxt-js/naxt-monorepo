import { defineConfig as _defineConfig, RollupOptions, Plugin as RollupPlugin } from "rollup";
import { dirname, resolve } from "path";
import { PackageJson, TsConfigJson } from "type-fest";
import glob from "glob";
import { existsSync } from "fs";
import _ from "lodash";
import yargs from "yargs";

import json from "@rollup/plugin-json";
import dts from "rollup-plugin-dts";
import typescript from "@rollup/plugin-typescript";

const isSelfPackage = (pkgPath: string) => (app: string) =>
  pkgPath.match(new RegExp(`(packages|libs)[/\\\\](${app}|${app.split("/")[1]})`));

const { app: apps } = yargs(process.argv).option("app", { type: "string", default: [], string: true, array: true }).parseSync();

const packages = glob
  .sync("{packages,libs}/*/package.json", { absolute: true })
  .filter(pkgPath => (apps.length ? apps.find(isSelfPackage(pkgPath)) : true));

if (!packages.length) {
  throw new Error("No packages found");
}

const configs = packages
  .map<RollupOptions[]>(pkgPath => {
    const root = dirname(pkgPath);
    const input = resolve(root, "src/index.ts");

    const pkg: PackageJson = require(pkgPath);
    const configs: RollupOptions[] = [];
    const tsConfigFilePath = resolve(root, "tsconfig.json");
    let tsConfig: TsConfigJson = { compilerOptions: { module: "ESNext", target: "ESNext" } };

    if (existsSync(tsConfigFilePath)) {
      const parsedTsConfig = require(tsConfigFilePath) || {};
      tsConfig = _.merge(tsConfig, parsedTsConfig);
    }

    const initialPlugins: RollupPlugin[] = [json()];

    if (pkg.main) {
      configs.push({
        onwarn() {},
        output: { file: resolve(root, pkg.main), format: "cjs", sourcemap: true },
        plugins: [...initialPlugins, typescript({ compilerOptions: tsConfig.compilerOptions })]
      });
    }

    if (pkg.module) {
      configs.push({
        onwarn() {},
        output: { file: resolve(root, pkg.module), format: "esm", sourcemap: true },
        plugins: [...initialPlugins, typescript({ compilerOptions: tsConfig.compilerOptions })]
      });
    }

    if (pkg.types) {
      configs.push({
        onwarn() {},
        output: { file: resolve(root, pkg.types), format: "esm", sourcemap: true },
        plugins: [...initialPlugins, dts()]
      });
    }

    return configs.map(config => ({ ...config, input }));
  })
  .flat();

export default _defineConfig(configs);
