import { PackageJson } from "type-fest";
import { mkdirSync, writeFileSync } from "fs";
import { resolve } from "path";
import { resolveConfig, format } from "prettier";

if (process.argv.length < 3) {
  console.log(`Usage: add-app <app-name> <app-type optional enum="library | package">`);
  process.exit(1);
}
const stringify = (obj: any) => JSON.stringify(obj, null, 2);

const appNames = process.argv[2] || "";
const appType = process.argv[3] || "package";

resolveConfig(resolve(process.cwd(), ".prettierrc")).then(prettierConfig => {
  appNames
    .split(",")
    .filter(Boolean)
    .forEach((appName, idx, self) => {
      const appPathDirectory = appType === "package" ? "packages" : "libs";
      const appPath = `${appPathDirectory}/${appName}`;
      const appPackageJson: PackageJson = {
        name: `@naxt/${appName}`,
        version: "0.0.0",
        description: "",
        main: "build/index.js",
        scripts: {},
        keywords: []
      };
      const appTsConfig = {
        extends: "../../tsconfig.base.json"
      };

      mkdirSync(resolve(process.cwd(), appPath, "src"), { recursive: true });

      console.log(`CREATE ${appPath}/package.json`);
      writeFileSync(
        resolve(appPath, "package.json"),
        format(stringify(appPackageJson), { ...prettierConfig, parser: "json" })
      );

      console.log(`CREATE ${appPath}/tsconfig.json`);
      writeFileSync(
        resolve(process.cwd(), appPath, "tsconfig.json"),
        format(stringify(appTsConfig), { ...prettierConfig, parser: "json" })
      );

      console.log(`CREATE ${appPath}/src/index.ts`);
      writeFileSync(resolve(appPath, "src/index.ts"), `export * from "./${appName}";`);

      console.log(`CREATE ${appPath}/src/${appName}.ts`);
      writeFileSync(
        resolve(appPath, "src", `${appName}.ts`),
        format(`export const ${appName} = () => {console.log("${appName}");}`, {
          ...prettierConfig,
          parser: "babel-ts"
        })
      );

      console.log(`UPDATE tsconfig.base.json`);
      const tsConfigBase = require("../tsconfig.base.json");
      tsConfigBase.compilerOptions.paths[`@naxt/${appName}`] = [`${appPath}/src`];
      writeFileSync(
        resolve(process.cwd(), "tsconfig.base.json"),
        format(stringify(tsConfigBase), { ...prettierConfig, parser: "json" })
      );

      idx !== self.length - 1 && console.log();
    });
});
