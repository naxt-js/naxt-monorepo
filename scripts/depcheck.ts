import { resolve } from "path";
import { unlinkSync, writeFileSync } from "fs";
import spawn from "cross-spawn";
import glob from "glob";

import { name, workspaces } from "../package.json";

const isDevDep = dep => dep.startsWith("@types") || dep === "@naxt/types" || dep.includes("type") || dep.startsWith("rollup-plugin-");

interface DepCheck {
  dependencies: string[];
  devDependencies: string[];
  missing: Record<string, string[]>;
  using: Record<string, string[]>;
  invalidFiles: Record<string, string[]>;
  invalidDirs: Record<string, string[]>;
}

const cwds = [".", ...workspaces].flatMap(workspace => glob.sync(workspace, { absolute: true, cwd: resolve(process.cwd()) }));
const addCommand = (isDev: boolean, repo: string, ...packages: string[]) =>
  `yarn ${repo !== name ? `workspace ${repo} ` : ``}add ${isDev ? "-D " : ""}${packages.join(" ")}`;

(async () => {
  const commands: string[] = [];
  const commandsTxt = resolve(process.cwd(), "missing-dependencies-install.txt");

  try {
    unlinkSync(commandsTxt);
  } catch (e) {}

  for (const cwd of cwds) {
    const repo = require(resolve(cwd, "package.json")).name;
    const data = spawn.sync("yarn", ["exec", "depcheck", "--json"], { cwd, stdio: "pipe" });
    const jsonString = data.output
      .map(x => x?.toString("utf-8") || "")
      .join("")
      .trim();

    try {
      const { missing } = JSON.parse(jsonString) as DepCheck;
      const dependencies = Object.keys(missing);

      if (dependencies.length) {
        const deps = dependencies.filter(dep => !isDevDep(dep));
        const devDeps = dependencies.filter(isDevDep);

        const missingJsonCommands = {
          dependency: (deps.length && addCommand(false, repo, ...deps)) || undefined,
          devDependency: (devDeps.length && addCommand(true, repo, ...devDeps)) || undefined
        };

        commands.push(missingJsonCommands.dependency, missingJsonCommands.devDependency);

        console.log(`[${repo}]: Found ${dependencies.length} missing dependencies`);
      } else {
        console.log(`[${repo}]: All dependencies are present`);
      }
    } catch (e) {
      console.log(e);
    }
  }

  writeFileSync(commandsTxt, commands.filter(Boolean).join(" & "));
})();
